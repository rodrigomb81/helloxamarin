﻿using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace HelloXamarin2
{
    class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        int _conteo = 0;
        bool _puedeIncrementar = true;

        private static int MAX = 99;

        public MainPageViewModel()
        {
            ComandoIncrementar = new Command(IncrementarConteo);
            ComandoResetear = new Command(ResetearContador);
        }

        public int Conteo
        {
            get =>_conteo;

            set
            {
                if (_conteo == value) return;
                if (value > MAX) return;

                _conteo = value;

                OnPropertyChanged(nameof(Conteo));
            }
        }

        public bool PuedeIncrementar {
            get => _puedeIncrementar;

            set
            {
                _puedeIncrementar = value;
                OnPropertyChanged(nameof(PuedeIncrementar));
            }
        }

        public ICommand ComandoIncrementar { get; }
        public ICommand ComandoResetear { get;  }

        void IncrementarConteo()
        {
            Conteo++;
            PuedeIncrementar = Conteo < MAX;
        }

        void ResetearContador()
        {
            Conteo = 0;
            PuedeIncrementar = true;
        }

        void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
